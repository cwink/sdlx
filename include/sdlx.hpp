#ifndef SDLX_HPP
#define SDLX_HPP

#include <functional>
#include <memory>
#include <stdexcept>
#include <utility>
#include <vector>

namespace sdlx {
using Error = std::runtime_error;

class Runtime final {
public:
  Runtime();
  Runtime(const Runtime &) noexcept = delete;
  Runtime(Runtime &&) noexcept = default;
  auto operator=(const Runtime &) noexcept -> Runtime & = delete;
  auto operator=(Runtime &&) noexcept -> Runtime & = default;
  ~Runtime() noexcept;
};

class Point final {
  double x_{0.0};
  double y_{0.0};

public:
  Point() noexcept = default;
  Point(const Point &) noexcept = default;
  Point(Point &&) noexcept = default;
  auto operator=(const Point &) noexcept -> Point & = default;
  auto operator=(Point &&) noexcept -> Point & = default;
  ~Point() noexcept = default;

  Point(double x, double y) noexcept;

  auto x() const noexcept -> double;
  auto y() const noexcept -> double;

  auto x(double x) noexcept -> void;
  auto y(double y) noexcept -> void;
};

using Points = std::vector<Point>;

auto operator==(const Point &left, const Point &right) noexcept -> bool;
auto operator!=(const Point &left, const Point &right) noexcept -> bool;
auto operator+(const Point &left, const Point &right) noexcept -> Point;
auto operator-(const Point &left, const Point &right) noexcept -> Point;
auto operator*(const Point &left, const Point &right) noexcept -> Point;
auto operator/(const Point &left, const Point &right) noexcept -> Point;
auto operator+=(Point &left, const Point &right) noexcept -> Point &;
auto operator-=(Point &left, const Point &right) noexcept -> Point &;
auto operator*=(Point &left, const Point &right) noexcept -> Point &;
auto operator/=(Point &left, const Point &right) noexcept -> Point &;

using Line = std::pair<Point, Point>;
using Lines = std::vector<Line>;

class Dimension final {
  std::size_t width_{0};
  std::size_t height_{0};

public:
  Dimension() noexcept = default;
  Dimension(const Dimension &) noexcept = default;
  Dimension(Dimension &&) noexcept = default;
  auto operator=(const Dimension &) noexcept -> Dimension & = default;
  auto operator=(Dimension &&) noexcept -> Dimension & = default;
  ~Dimension() noexcept = default;

  Dimension(std::size_t width, std::size_t height) noexcept;

  auto width() const noexcept -> std::size_t;
  auto height() const noexcept -> std::size_t;

  auto width(std::size_t width) noexcept -> void;
  auto height(std::size_t height) noexcept -> void;
};

using Dimensions = std::vector<Dimension>;

auto operator==(const Dimension &left, const Dimension &right) noexcept -> bool;
auto operator!=(const Dimension &left, const Dimension &right) noexcept -> bool;

class Rectangle final {
  Point point_ = Point();
  Dimension dimension_ = Dimension();

public:
  Rectangle() noexcept = default;
  Rectangle(const Rectangle &) noexcept = default;
  Rectangle(Rectangle &&) noexcept = default;
  auto operator=(const Rectangle &) noexcept -> Rectangle & = default;
  auto operator=(Rectangle &&) noexcept -> Rectangle & = default;
  ~Rectangle() noexcept = default;

  Rectangle(const Point &point, const Dimension &dimension) noexcept;

  auto point() const noexcept -> const Point &;
  auto point() noexcept -> Point &;
  auto dimension() const noexcept -> const Dimension &;
  auto dimension() noexcept -> Dimension &;

  auto point(const Point &point) noexcept -> void;
  auto dimension(const Dimension &dimension) noexcept -> void;
};

using Rectangles = std::vector<Rectangle>;

auto operator==(const Rectangle &left, const Rectangle &right) noexcept -> bool;
auto operator!=(const Rectangle &left, const Rectangle &right) noexcept -> bool;

auto intersect(const Rectangle &left, const Rectangle &right) noexcept -> bool;

class Color final {
  std::uint8_t red_{0};
  std::uint8_t green_{0};
  std::uint8_t blue_{0};
  std::uint8_t alpha_{255};

public:
  Color() noexcept = default;
  Color(const Color &) noexcept = default;
  Color(Color &&) noexcept = default;
  auto operator=(const Color &) noexcept -> Color & = default;
  auto operator=(Color &&) noexcept -> Color & = default;
  ~Color() noexcept = default;

  Color(std::uint8_t red, std::uint8_t green, std::uint8_t blue, std::uint8_t alpha) noexcept;
  Color(std::uint8_t red, std::uint8_t green, std::uint8_t blue) noexcept;

  auto red() const noexcept -> std::uint8_t;
  auto green() const noexcept -> std::uint8_t;
  auto blue() const noexcept -> std::uint8_t;
  auto alpha() const noexcept -> std::uint8_t;

  auto red(std::uint8_t red) noexcept -> void;
  auto green(std::uint8_t green) noexcept -> void;
  auto blue(std::uint8_t blue) noexcept -> void;
  auto alpha(std::uint8_t alpha) noexcept -> void;
};

auto operator==(const Color &left, const Color &right) noexcept -> bool;
auto operator!=(const Color &left, const Color &right) noexcept -> bool;

constexpr auto pi = 3.14159265359;

auto degrees(double radians) noexcept -> double;
auto radians(double degrees) noexcept -> double;
auto delay(std::size_t milliseconds) -> void;

class Display final {
  class Impl;
  std::shared_ptr<Impl> impl_ = std::shared_ptr<Impl>(nullptr);

public:
  Display() noexcept = delete;
  Display(const Display &) noexcept = delete;
  Display(Display &&) noexcept = default;
  auto operator=(const Display &) noexcept -> Display & = delete;
  auto operator=(Display &&) noexcept -> Display & = default;
  ~Display() noexcept = default;

  Display(const std::string &title, const Dimension &dimension);

  auto impl() const noexcept -> const Impl &;
  auto impl() noexcept -> Impl &;
};

auto clear(Display &display, const Color &color) -> void;
auto present(Display &display) -> void;
auto draw(Display &display, const Point &point, const Color &color) -> void;
auto draw(Display &display, const Points &points, const Color &color) -> void;
auto draw(Display &display, const Line &line, const Color &color) -> void;
auto draw(Display &display, const Lines &lines, const Color &color) -> void;
auto draw(Display &display, const Rectangle &rectangle, const Color &color, bool filled = false) -> void;
auto draw(Display &display, const Rectangles &rectangles, const Color &color, bool filled = false) -> void;
auto draw(Display &display, const Point &point, double radius, const Color &color, bool filled = false) -> void;

class Texture final {
  class Impl;
  std::shared_ptr<Impl> impl_ = std::shared_ptr<Impl>(nullptr);

public:
  Texture() noexcept = delete;
  Texture(const Texture &) noexcept = delete;
  Texture(Texture &&) noexcept = default;
  auto operator=(const Texture &) noexcept -> Texture & = delete;
  auto operator=(Texture &&) noexcept -> Texture & = default;
  ~Texture() noexcept = default;

  Texture(Display &display, const std::string &file);

  auto impl() const noexcept -> const Impl &;
  auto impl() noexcept -> Impl &;
  auto dimension() const noexcept -> const Dimension &;
};

auto draw(Display &display, Texture &texture, const Rectangle &source, const Rectangle &destination, double angle,
          const Point &center, bool blended = false) -> void;
auto draw(Display &display, Texture &texture, const Point &point, double angle, double scale, bool blended = false)
    -> void;

enum class Key_state : std::uint8_t { inactive, up, down };

enum class Key : std::uint8_t {
  inactive,
  zero,
  one,
  two,
  three,
  four,
  five,
  six,
  seven,
  eight,
  nine,
  a,
  b,
  c,
  d,
  e,
  f,
  g,
  h,
  i,
  j,
  k,
  l,
  m,
  n,
  o,
  p,
  q,
  r,
  s,
  t,
  u,
  v,
  w,
  x,
  y,
  z,
  space,
  escape
};

auto pressed(Key key) -> bool;

class Messages final {
  bool window_closing_{false};
  Key_state key_state_{Key_state::inactive};
  Key key_{Key::inactive};

public:
  Messages() noexcept = default;
  Messages(const Messages &) noexcept = delete;
  Messages(Messages &&) noexcept = default;
  auto operator=(const Messages &) noexcept -> Messages & = delete;
  auto operator=(Messages &&) noexcept -> Messages & = default;
  ~Messages() noexcept = default;

  auto window_closing() const noexcept -> bool;
  auto key_state() const noexcept -> Key_state;
  auto key() const noexcept -> Key;

  auto poll() noexcept -> void;
};

using Integrator = std::function<void(const double, const double)>;

class Timestep final {
  double time_{0.0};
  double delta_time_{0.01};
  double current_time_{0.0};
  double accumulator_{0.0};

public:
  Timestep() noexcept;
  Timestep(const Timestep &) noexcept = delete;
  Timestep(Timestep &&) noexcept = default;
  auto operator=(const Timestep &) noexcept -> Timestep & = delete;
  auto operator=(Timestep &&) noexcept -> Timestep & = default;
  ~Timestep() noexcept = default;

  explicit Timestep(double delta_time) noexcept;

  auto update() noexcept -> void;
  auto integrate(const Integrator &integrator) noexcept -> void;
};

class Sound final {
  class Impl;
  std::shared_ptr<Impl> impl_ = std::shared_ptr<Impl>(nullptr);

public:
  Sound() noexcept = delete;
  Sound(const Sound &) noexcept = delete;
  Sound(Sound &&) noexcept = default;
  auto operator=(const Sound &) noexcept -> Sound & = delete;
  auto operator=(Sound &&) noexcept -> Sound & = default;
  ~Sound() noexcept = default;

  explicit Sound(const std::string &file);

  auto impl() const noexcept -> const Impl &;
  auto impl() noexcept -> Impl &;
};

using Channel = std::uint32_t;

auto volume(Sound &sound, std::size_t volume) noexcept -> void;
auto play(Sound &sound, std::int32_t loops = 0) -> Channel;
auto play_timed(Sound &sound, std::size_t milliseconds) -> Channel;
auto pause(Channel channel) noexcept -> void;
auto resume(Channel channel) noexcept -> void;
auto stop(Channel channel) noexcept -> void;
auto pause_all_channels() noexcept -> void;
auto resume_all_channels() noexcept -> void;
auto stop_all_channels() noexcept -> void;
auto playing(Channel channel) noexcept -> bool;

class Font final {
  class Impl;
  std::shared_ptr<Impl> impl_ = std::shared_ptr<Impl>(nullptr);

public:
  Font() noexcept = delete;
  Font(const Font &) noexcept = delete;
  Font(Font &&) noexcept = default;
  auto operator=(const Font &) noexcept -> Font & = delete;
  auto operator=(Font &&) noexcept -> Font & = default;
  ~Font() noexcept = default;

  Font(const std::string &file, std::size_t size);

  auto impl() const noexcept -> const Impl &;
  auto impl() noexcept -> Impl &;
};

auto draw(Display &display, Font &font, const std::string &text, const Color &color, const Rectangle &source,
          const Rectangle &destination, double angle, const Point &center, bool blended = false) -> void;
auto draw(Display &display, Font &font, const std::string &text, const Color &color, const Point &point, double angle,
          double scale, bool blended = false) -> void;
} // namespace sdlx

#endif // SDLX_HPP
