#include "include/sdlx.hpp"

#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) {
  auto runtime = sdlx::Runtime();
  auto display = sdlx::Display("LOL", {1024, 768});
  auto messages = sdlx::Messages();
  auto timestep = sdlx::Timestep();

  auto smile = sdlx::Texture(display, "assets/smile.png");

  auto laser = sdlx::Sound("assets/laser.ogg");
  sdlx::volume(laser, 50);

  auto roboto = sdlx::Font("assets/roboto.ttf", 24);

  auto rotation = 0.0;

  for (;;) {
    timestep.update();

    messages.poll();

    if (messages.window_closing() || sdlx::pressed(sdlx::Key::escape)) {
      break;
    }

    if (messages.key_state() == sdlx::Key_state::up && messages.key() == sdlx::Key::space) {
      sdlx::play(laser);
    }

    timestep.integrate([&](const auto, const auto delta_time) {
      rotation += 500.0 * delta_time;

      if (rotation > 360.0) {
        rotation = 0.0;
      }
    });

    sdlx::clear(display, {0, 0, 0});

    sdlx::draw(display, smile, {1024.0 / 2.0, 768.0 / 2.0}, sdlx::radians(rotation), 0.25);

    sdlx::draw(display, roboto, "Testing 123...", {255, 0, 0}, {100.0, 100.0}, sdlx::radians(rotation), 1.0);

    sdlx::draw(display, {400.0, 400.0}, 50.0, {0, 255, 0});

    sdlx::present(display);

    sdlx::delay(1);
  }

  return 0;
}
