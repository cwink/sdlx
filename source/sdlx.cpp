#include "../include/sdlx.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include <cmath>

namespace {
auto scancode(const sdlx::Key key) -> SDL_Scancode {
  switch (key) {
  case sdlx::Key::inactive:
    throw sdlx::Error("Unable to obtain scancode for inactive key,");
  case sdlx::Key::zero:
    return SDL_SCANCODE_0;
  case sdlx::Key::one:
    return SDL_SCANCODE_1;
  case sdlx::Key::two:
    return SDL_SCANCODE_2;
  case sdlx::Key::three:
    return SDL_SCANCODE_3;
  case sdlx::Key::four:
    return SDL_SCANCODE_4;
  case sdlx::Key::five:
    return SDL_SCANCODE_5;
  case sdlx::Key::six:
    return SDL_SCANCODE_6;
  case sdlx::Key::seven:
    return SDL_SCANCODE_7;
  case sdlx::Key::eight:
    return SDL_SCANCODE_8;
  case sdlx::Key::nine:
    return SDL_SCANCODE_9;
  case sdlx::Key::a:
    return SDL_SCANCODE_A;
  case sdlx::Key::b:
    return SDL_SCANCODE_B;
  case sdlx::Key::c:
    return SDL_SCANCODE_C;
  case sdlx::Key::d:
    return SDL_SCANCODE_D;
  case sdlx::Key::e:
    return SDL_SCANCODE_E;
  case sdlx::Key::f:
    return SDL_SCANCODE_F;
  case sdlx::Key::g:
    return SDL_SCANCODE_G;
  case sdlx::Key::h:
    return SDL_SCANCODE_H;
  case sdlx::Key::i:
    return SDL_SCANCODE_I;
  case sdlx::Key::j:
    return SDL_SCANCODE_J;
  case sdlx::Key::k:
    return SDL_SCANCODE_K;
  case sdlx::Key::l:
    return SDL_SCANCODE_L;
  case sdlx::Key::m:
    return SDL_SCANCODE_M;
  case sdlx::Key::n:
    return SDL_SCANCODE_N;
  case sdlx::Key::o:
    return SDL_SCANCODE_O;
  case sdlx::Key::p:
    return SDL_SCANCODE_P;
  case sdlx::Key::q:
    return SDL_SCANCODE_Q;
  case sdlx::Key::r:
    return SDL_SCANCODE_R;
  case sdlx::Key::s:
    return SDL_SCANCODE_S;
  case sdlx::Key::t:
    return SDL_SCANCODE_T;
  case sdlx::Key::u:
    return SDL_SCANCODE_U;
  case sdlx::Key::v:
    return SDL_SCANCODE_V;
  case sdlx::Key::w:
    return SDL_SCANCODE_W;
  case sdlx::Key::x:
    return SDL_SCANCODE_X;
  case sdlx::Key::y:
    return SDL_SCANCODE_Y;
  case sdlx::Key::z:
    return SDL_SCANCODE_Z;
  case sdlx::Key::space:
    return SDL_SCANCODE_SPACE;
  case sdlx::Key::escape:
    return SDL_SCANCODE_ESCAPE;
  }
}

auto key(const SDL_Scancode scancode) -> sdlx::Key {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch-enum"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch-enum"
  switch (scancode) {
  case SDL_SCANCODE_0:
    return sdlx::Key::zero;
  case SDL_SCANCODE_1:
    return sdlx::Key::one;
  case SDL_SCANCODE_2:
    return sdlx::Key::two;
  case SDL_SCANCODE_3:
    return sdlx::Key::three;
  case SDL_SCANCODE_4:
    return sdlx::Key::four;
  case SDL_SCANCODE_5:
    return sdlx::Key::five;
  case SDL_SCANCODE_6:
    return sdlx::Key::six;
  case SDL_SCANCODE_7:
    return sdlx::Key::seven;
  case SDL_SCANCODE_8:
    return sdlx::Key::eight;
  case SDL_SCANCODE_9:
    return sdlx::Key::nine;
  case SDL_SCANCODE_A:
    return sdlx::Key::a;
  case SDL_SCANCODE_B:
    return sdlx::Key::b;
  case SDL_SCANCODE_C:
    return sdlx::Key::c;
  case SDL_SCANCODE_D:
    return sdlx::Key::d;
  case SDL_SCANCODE_E:
    return sdlx::Key::e;
  case SDL_SCANCODE_F:
    return sdlx::Key::f;
  case SDL_SCANCODE_G:
    return sdlx::Key::g;
  case SDL_SCANCODE_H:
    return sdlx::Key::h;
  case SDL_SCANCODE_I:
    return sdlx::Key::i;
  case SDL_SCANCODE_J:
    return sdlx::Key::j;
  case SDL_SCANCODE_K:
    return sdlx::Key::k;
  case SDL_SCANCODE_L:
    return sdlx::Key::l;
  case SDL_SCANCODE_M:
    return sdlx::Key::m;
  case SDL_SCANCODE_N:
    return sdlx::Key::n;
  case SDL_SCANCODE_O:
    return sdlx::Key::o;
  case SDL_SCANCODE_P:
    return sdlx::Key::p;
  case SDL_SCANCODE_Q:
    return sdlx::Key::q;
  case SDL_SCANCODE_R:
    return sdlx::Key::r;
  case SDL_SCANCODE_S:
    return sdlx::Key::s;
  case SDL_SCANCODE_T:
    return sdlx::Key::t;
  case SDL_SCANCODE_U:
    return sdlx::Key::u;
  case SDL_SCANCODE_V:
    return sdlx::Key::v;
  case SDL_SCANCODE_W:
    return sdlx::Key::w;
  case SDL_SCANCODE_X:
    return sdlx::Key::x;
  case SDL_SCANCODE_Y:
    return sdlx::Key::y;
  case SDL_SCANCODE_Z:
    return sdlx::Key::z;
  case SDL_SCANCODE_SPACE:
    return sdlx::Key::space;
  case SDL_SCANCODE_ESCAPE:
    return sdlx::Key::escape;
  default:
    return sdlx::Key::inactive;
  }
#pragma GCC diagnostic pop
#pragma clang diagnostic pop
}
} // namespace

namespace sdlx {
Runtime::Runtime() {
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    throw Error(SDL_GetError());
  }

  const auto img_flags = IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF; // NOLINT
  const auto img_init = IMG_Init(img_flags);
  if ((img_init & img_flags) != img_flags) { // NOLINT
    throw Error(IMG_GetError());
  }

  if (TTF_Init() == -1) {
    throw Error(TTF_GetError());
  }

  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) < 0) {
    throw Error(Mix_GetError());
  }

  const auto mix_flags = MIX_INIT_FLAC | MIX_INIT_MOD | MIX_INIT_MP3 | MIX_INIT_OGG; // NOLINT
  const auto mix_init = Mix_Init(mix_flags);
  if ((mix_init & mix_flags) != mix_flags) { // NOLINT
    throw Error(Mix_GetError());
  }

  Mix_AllocateChannels(32);
}

Runtime::~Runtime() noexcept {
  Mix_Quit();
  Mix_CloseAudio();
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

Point::Point(const double x, const double y) noexcept : x_{x}, y_{y} {}

auto Point::x() const noexcept -> double { return x_; }

auto Point::y() const noexcept -> double { return y_; }

auto Point::x(const double x) noexcept -> void { x_ = x; }

auto Point::y(const double y) noexcept -> void { y_ = y; }

auto operator==(const Point &left, const Point &right) noexcept -> bool {
  return std::fabs(left.x() - right.x()) < 0.00001 && std::fabs(left.y() - right.y()) < 0.00001;
}

auto operator!=(const Point &left, const Point &right) noexcept -> bool { return !(left == right); }

auto operator+(const Point &left, const Point &right) noexcept -> Point {
  return Point(left.x() + right.x(), left.y() + right.y());
}

auto operator-(const Point &left, const Point &right) noexcept -> Point {
  return Point(left.x() - right.x(), left.y() - right.y());
}

auto operator*(const Point &left, const Point &right) noexcept -> Point {
  return Point(left.x() * right.x(), left.y() * right.y());
}

auto operator/(const Point &left, const Point &right) noexcept -> Point {
  return Point(left.x() / right.x(), left.y() / right.y());
}

auto operator+=(Point &left, const Point &right) noexcept -> Point & {
  left.x(left.x() + right.x());
  left.y(left.y() + right.y());

  return left;
}

auto operator-=(Point &left, const Point &right) noexcept -> Point & {
  left.x(left.x() - right.x());
  left.y(left.y() - right.y());

  return left;
}

auto operator*=(Point &left, const Point &right) noexcept -> Point & {
  left.x(left.x() * right.x());
  left.y(left.y() * right.y());

  return left;
}

auto operator/=(Point &left, const Point &right) noexcept -> Point & {
  left.x(left.x() / right.x());
  left.y(left.y() / right.y());

  return left;
}

Dimension::Dimension(const std::size_t width, const std::size_t height) noexcept : width_{width}, height_{height} {}

auto Dimension::width() const noexcept -> std::size_t { return width_; }

auto Dimension::height() const noexcept -> std::size_t { return height_; }

auto Dimension::width(const std::size_t width) noexcept -> void { width_ = width; }

auto Dimension::height(const std::size_t height) noexcept -> void { height_ = height; }

auto operator==(const Dimension &left, const Dimension &right) noexcept -> bool {
  return left.width() == right.width() && left.height() == right.height();
}

auto operator!=(const Dimension &left, const Dimension &right) noexcept -> bool { return !(left == right); }

Rectangle::Rectangle(const Point &point, const Dimension &dimension) noexcept : point_(point), dimension_(dimension) {}

auto Rectangle::point() const noexcept -> const Point & { return point_; }

auto Rectangle::point() noexcept -> Point & { return point_; }

auto Rectangle::dimension() const noexcept -> const Dimension & { return dimension_; }

auto Rectangle::dimension() noexcept -> Dimension & { return dimension_; }

auto Rectangle::point(const Point &point) noexcept -> void { point_ = point; }

auto Rectangle::dimension(const Dimension &dimension) noexcept -> void { dimension_ = dimension; }

auto operator==(const Rectangle &left, const Rectangle &right) noexcept -> bool {
  return left.point() == right.point() && left.dimension() == right.dimension();
}

auto operator!=(const Rectangle &left, const Rectangle &right) noexcept -> bool { return !(left == right); }

auto intersect(const Rectangle &left, const Rectangle &right) noexcept -> bool {
  const auto first = SDL_Rect{static_cast<int>(left.point().x()), static_cast<int>(left.point().y()),
                              static_cast<int>(left.dimension().width()), static_cast<int>(left.dimension().height())};

  const auto second =
      SDL_Rect{static_cast<int>(right.point().x()), static_cast<int>(right.point().y()),
               static_cast<int>(right.dimension().width()), static_cast<int>(right.dimension().height())};

  return SDL_HasIntersection(&first, &second) == SDL_TRUE;
}

Color::Color(const std::uint8_t red, const std::uint8_t green, const std::uint8_t blue,
             const std::uint8_t alpha) noexcept
    : red_{red}, green_{green}, blue_{blue}, alpha_{alpha} {}

Color::Color(const std::uint8_t red, const std::uint8_t green, const std::uint8_t blue) noexcept
    : red_{red}, green_{green}, blue_{blue} {}

auto Color::red() const noexcept -> std::uint8_t { return red_; }

auto Color::green() const noexcept -> std::uint8_t { return green_; }

auto Color::blue() const noexcept -> std::uint8_t { return blue_; }

auto Color::alpha() const noexcept -> std::uint8_t { return alpha_; }

auto Color::red(const std::uint8_t red) noexcept -> void { red_ = red; }

auto Color::green(const std::uint8_t green) noexcept -> void { green_ = green; }

auto Color::blue(const std::uint8_t blue) noexcept -> void { blue_ = blue; }

auto Color::alpha(const std::uint8_t alpha) noexcept -> void { alpha_ = alpha; }

auto operator==(const Color &left, const Color &right) noexcept -> bool {
  return left.red() == right.red() && left.green() == right.green() && left.blue() == right.blue() &&
         left.alpha() == right.alpha();
}

auto operator!=(const Color &left, const Color &right) noexcept -> bool { return !(left == right); }

auto degrees(const double radians) noexcept -> double { return radians * 180.0 / pi; }

auto radians(const double degrees) noexcept -> double { return degrees * pi / 180.0; }

auto delay(const std::size_t milliseconds) -> void { SDL_Delay(static_cast<Uint32>(milliseconds)); }

class Display::Impl final {
  SDL_Window *window_{nullptr};
  SDL_Renderer *renderer_{nullptr};

public:
  Impl() noexcept = delete;
  Impl(const Impl &) noexcept = delete;
  Impl(Impl &&) noexcept = default;
  auto operator=(const Impl &) noexcept -> Impl & = delete;
  auto operator=(Impl &&) noexcept -> Impl & = delete;
  ~Impl() noexcept;

  Impl(const std::string &title, const Dimension &dimension);

  auto window() const noexcept -> const SDL_Window *;
  auto window() noexcept -> SDL_Window *;
  auto renderer() const noexcept -> const SDL_Renderer *;
  auto renderer() noexcept -> SDL_Renderer *;
};

Display::Impl::~Impl() noexcept {
  SDL_DestroyRenderer(renderer_);
  SDL_DestroyWindow(window_);
}

Display::Impl::Impl(const std::string &title, const Dimension &dimension) {
  window_ =
      SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, // NOLINT
                       static_cast<int>(dimension.width()), static_cast<int>(dimension.height()), SDL_WINDOW_OPENGL);

  if (window_ == nullptr) {
    throw Error(SDL_GetError());
  }

  renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);

  if (renderer_ == nullptr) {
    throw Error(SDL_GetError());
  }

  const auto sdl_rect = SDL_Rect{0, 0, static_cast<int>(dimension.width()), static_cast<int>(dimension.height())};

  if (SDL_RenderSetViewport(renderer_, &sdl_rect) < 0) {
    throw Error(SDL_GetError());
  }

  if (SDL_RenderSetLogicalSize(renderer_, static_cast<int>(dimension.width()), static_cast<int>(dimension.height())) <
      0) {
    throw Error(SDL_GetError());
  }

  if (SDL_RenderSetScale(renderer_, 1.0f, 1.0f) < 0) {
    throw Error(SDL_GetError());
  }

  if (SDL_SetHintWithPriority(SDL_HINT_RENDER_SCALE_QUALITY, "0", SDL_HINT_OVERRIDE) != SDL_TRUE) {
    throw Error("Failed to set SDL_HINT_RENDER_SCALE_QUALITY to 0.");
  }
}

auto Display::Impl::window() const noexcept -> const SDL_Window * { return window_; }

auto Display::Impl::window() noexcept -> SDL_Window * { return window_; }

auto Display::Impl::renderer() const noexcept -> const SDL_Renderer * { return renderer_; }

auto Display::Impl::renderer() noexcept -> SDL_Renderer * { return renderer_; }

Display::Display(const std::string &title, const Dimension &dimension) : impl_(new Impl(title, dimension)) {}

auto Display::impl() const noexcept -> const Impl & { return *(impl_.get()); }

auto Display::impl() noexcept -> Impl & { return *(impl_.get()); }

auto clear(Display &display, const Color &color) -> void {
  if (SDL_SetRenderDrawColor(display.impl().renderer(), color.red(), color.green(), color.blue(), color.alpha()) < 0) {
    throw Error(SDL_GetError());
  }

  if (SDL_RenderClear(display.impl().renderer()) < 0) {
    throw Error(SDL_GetError());
  }
}

auto present(Display &display) -> void { SDL_RenderPresent(display.impl().renderer()); }

auto draw(Display &display, const Point &point, const Color &color) -> void {
  if (SDL_SetRenderDrawColor(display.impl().renderer(), color.red(), color.green(), color.blue(), color.alpha()) < 0) {
    throw Error(SDL_GetError());
  }

  if (SDL_RenderDrawPoint(display.impl().renderer(), static_cast<int>(point.x()), static_cast<int>(point.y())) < 0) {
    throw Error(SDL_GetError());
  }
}

auto draw(Display &display, const Points &points, const Color &color) -> void {
  if (SDL_SetRenderDrawColor(display.impl().renderer(), color.red(), color.green(), color.blue(), color.alpha()) < 0) {
    throw Error(SDL_GetError());
  }

  auto sdl_points = std::vector<SDL_Point>();

  for (const auto &point : points) {
    sdl_points.emplace_back(SDL_Point{static_cast<int>(point.x()), static_cast<int>(point.y())});
  }

  if (SDL_RenderDrawPoints(display.impl().renderer(), sdl_points.data(), static_cast<int>(sdl_points.size())) < 0) {
    throw Error(SDL_GetError());
  }
}

auto draw(Display &display, const Line &line, const Color &color) -> void {
  if (SDL_SetRenderDrawColor(display.impl().renderer(), color.red(), color.green(), color.blue(), color.alpha()) < 0) {
    throw Error(SDL_GetError());
  }

  if (SDL_RenderDrawLine(display.impl().renderer(), static_cast<int>(line.first.x()), static_cast<int>(line.first.y()),
                         static_cast<int>(line.second.x()), static_cast<int>(line.second.y())) < 0) {
    throw Error(SDL_GetError());
  }
}

auto draw(Display &display, const Lines &lines, const Color &color) -> void {
  if (SDL_SetRenderDrawColor(display.impl().renderer(), color.red(), color.green(), color.blue(), color.alpha()) < 0) {
    throw Error(SDL_GetError());
  }

  auto sdl_points = std::vector<SDL_Point>();

  for (const auto &line : lines) {
    sdl_points.emplace_back(SDL_Point{static_cast<int>(line.first.x()), static_cast<int>(line.first.y())});
    sdl_points.emplace_back(SDL_Point{static_cast<int>(line.second.x()), static_cast<int>(line.second.y())});
  }

  if (SDL_RenderDrawLines(display.impl().renderer(), sdl_points.data(), static_cast<int>(sdl_points.size())) < 0) {
    throw Error(SDL_GetError());
  }
}

auto draw(Display &display, const Rectangle &rectangle, const Color &color, bool filled) -> void {
  if (SDL_SetRenderDrawColor(display.impl().renderer(), color.red(), color.green(), color.blue(), color.alpha()) < 0) {
    throw Error(SDL_GetError());
  }

  const auto sdl_rect =
      SDL_Rect{static_cast<int>(rectangle.point().x()), static_cast<int>(rectangle.point().y()),
               static_cast<int>(rectangle.dimension().width()), static_cast<int>(rectangle.dimension().height())};

  if (filled) {
    if (SDL_RenderFillRect(display.impl().renderer(), &sdl_rect) < 0) {
      throw Error(SDL_GetError());
    }
  } else {
    if (SDL_RenderDrawRect(display.impl().renderer(), &sdl_rect) < 0) {
      throw Error(SDL_GetError());
    }
  }
}

auto draw(Display &display, const Rectangles &rectangles, const Color &color, bool filled) -> void {
  if (SDL_SetRenderDrawColor(display.impl().renderer(), color.red(), color.green(), color.blue(), color.alpha()) < 0) {
    throw Error(SDL_GetError());
  }

  auto sdl_rects = std::vector<SDL_Rect>();

  for (const auto &rectangle : rectangles) {
    sdl_rects.emplace_back(SDL_Rect{static_cast<int>(rectangle.point().x()), static_cast<int>(rectangle.point().y()),
                                    static_cast<int>(rectangle.dimension().width()),
                                    static_cast<int>(rectangle.dimension().height())});
  }

  if (filled) {
    if (SDL_RenderFillRects(display.impl().renderer(), sdl_rects.data(), static_cast<int>(sdl_rects.size())) < 0) {
      throw Error(SDL_GetError());
    }
  } else {
    if (SDL_RenderDrawRects(display.impl().renderer(), sdl_rects.data(), static_cast<int>(sdl_rects.size())) < 0) {
      throw Error(SDL_GetError());
    }
  }
}

auto draw(Display &display, const Point &point, const double radius, const Color &color, const bool filled) -> void {
  auto x = radius - 1.0;
  auto y = 0.0;
  auto dx = 1.0;
  auto dy = 1.0;
  auto error = static_cast<double>(dx - (static_cast<int>(radius) << 1));

  while (x >= y) {
    if (filled) {
      draw(display, Line({{point.x() - x, point.y() - y}, {point.x() + x, point.y() - y}}), color);
      draw(display, Line({{point.x() - y, point.y() + x}, {point.x() + y, point.y() + x}}), color);
      draw(display, Line({{point.x() - y, point.y() - x}, {point.x() + y, point.y() - x}}), color);
      draw(display, Line({{point.x() - x, point.y() + y}, {point.x() + x, point.y() + y}}), color);
    } else {
      draw(display, {point.x() + x, point.y() + y}, color);
      draw(display, {point.x() + y, point.y() + x}, color);
      draw(display, {point.x() - y, point.y() + x}, color);
      draw(display, {point.x() - x, point.y() + y}, color);
      draw(display, {point.x() - x, point.y() - y}, color);
      draw(display, {point.x() - y, point.y() - x}, color);
      draw(display, {point.x() + y, point.y() - x}, color);
      draw(display, {point.x() + x, point.y() - y}, color);
    }

    if (error <= 0.0) {
      ++y;
      error += dy;
      dy += 2.0;
    }

    if (error > 0.0) {
      --x;
      dx += 2.0;
      error += static_cast<double>(dx - (static_cast<int>(radius) << 1));
    }
  }
}

class Texture::Impl final {
  SDL_Texture *texture_{nullptr};
  Dimension dimension_ = Dimension();

public:
  Impl() noexcept = delete;
  Impl(const Impl &) noexcept = delete;
  Impl(Impl &&) noexcept = default;
  auto operator=(const Impl &) noexcept -> Impl & = delete;
  auto operator=(Impl &&) noexcept -> Impl & = default;
  ~Impl() noexcept;

  Impl(Display &display, const std::string &file);

  auto texture() const noexcept -> const SDL_Texture *;
  auto texture() noexcept -> SDL_Texture *;
  auto dimension() const noexcept -> const Dimension &;
};

Texture::Impl::Impl(Display &display, const std::string &file) {
  auto surface = IMG_Load(file.c_str());

  if (surface == nullptr) {
    throw Error(IMG_GetError());
  }

  dimension_ = Dimension(static_cast<std::size_t>(surface->w), static_cast<std::size_t>(surface->h));

  texture_ = SDL_CreateTextureFromSurface(display.impl().renderer(), surface);

  SDL_FreeSurface(surface);

  if (texture_ == nullptr) {
    throw Error(SDL_GetError());
  }
}

Texture::Impl::~Impl() noexcept { SDL_DestroyTexture(texture_); }

auto Texture::Impl::texture() const noexcept -> const SDL_Texture * { return texture_; }

auto Texture::Impl::texture() noexcept -> SDL_Texture * { return texture_; }

auto Texture::Impl::dimension() const noexcept -> const Dimension & { return dimension_; }

Texture::Texture(Display &display, const std::string &file) : impl_(new Impl(display, file)) {}

auto Texture::impl() const noexcept -> const Impl & { return *(impl_.get()); }

auto Texture::impl() noexcept -> Impl & { return *(impl_.get()); }

auto Texture::dimension() const noexcept -> const Dimension & { return impl_.get()->dimension(); }

auto draw(Display &display, Texture &texture, const Rectangle &source, const Rectangle &destination, const double angle,
          const Point &center, const bool blended) -> void {
  const auto sdl_source =
      SDL_Rect{static_cast<int>(source.point().x()), static_cast<int>(source.point().y()),
               static_cast<int>(source.dimension().width()), static_cast<int>(source.dimension().height())};

  const auto sdl_destination =
      SDL_Rect{static_cast<int>(destination.point().x()), static_cast<int>(destination.point().y()),
               static_cast<int>(destination.dimension().width()), static_cast<int>(destination.dimension().height())};

  const auto sdl_center = SDL_Point{static_cast<int>(center.x()), static_cast<int>(center.y())};

  if (blended) {
    if (SDL_SetRenderDrawBlendMode(display.impl().renderer(), SDL_BLENDMODE_BLEND) < 0) {
      throw Error(SDL_GetError());
    }
  } else {
    if (SDL_SetRenderDrawBlendMode(display.impl().renderer(), SDL_BLENDMODE_NONE) < 0) {
      throw Error(SDL_GetError());
    }
  }

  if (SDL_RenderCopyEx(display.impl().renderer(), texture.impl().texture(), &sdl_source, &sdl_destination,
                       degrees(angle), &sdl_center, SDL_FLIP_NONE) < 0) {
    throw Error(SDL_GetError());
  }
}

auto draw(Display &display, Texture &texture, const Point &point, const double angle, const double scale,
          const bool blended) -> void {
  const auto source = Rectangle({0.0, 0.0}, {texture.dimension().width(), texture.dimension().height()});
  const auto scaled = Dimension({static_cast<std::size_t>(texture.dimension().width() * scale),
                                 static_cast<std::size_t>(texture.dimension().height() * scale)});
  const auto center = Point(scaled.width() / 2.0, scaled.height() / 2.0);
  const auto destination = Rectangle(point - Point(scaled.width() / 2.0, scaled.height() / 2.0), scaled);

  draw(display, texture, source, destination, angle, center, blended);
}

auto pressed(const Key key) -> bool { return SDL_GetKeyboardState(nullptr)[scancode(key)] == 1; } // NOLINT

auto Messages::window_closing() const noexcept -> bool { return window_closing_; }

auto Messages::key_state() const noexcept -> Key_state { return key_state_; }

auto Messages::key() const noexcept -> Key { return key_; }

auto Messages::poll() noexcept -> void {
  window_closing_ = false;
  key_state_ = Key_state::inactive;
  key_ = Key::inactive;

  auto event = SDL_Event{0};

  while (SDL_PollEvent(&event) == 1) {
    switch (event.type) { // NOLINT
    case SDL_WINDOWEVENT:
      switch (event.window.event) { // NOLINT
      case SDL_WINDOWEVENT_CLOSE:
        window_closing_ = true;
        break;
      }
      break;
    case SDL_KEYUP:
      key_state_ = Key_state::up;
      key_ = ::key(event.key.keysym.scancode); // NOLINT
      break;
    case SDL_KEYDOWN:
      key_state_ = Key_state::down;
      key_ = ::key(event.key.keysym.scancode); // NOLINT
      break;
    }
  }
}

Timestep::Timestep() noexcept : current_time_{SDL_GetTicks() / 1000.0} {}

Timestep::Timestep(const double delta_time) noexcept
    : delta_time_{delta_time}, current_time_{SDL_GetTicks() / 1000.0} {}

auto Timestep::update() noexcept -> void {
  const auto new_time = SDL_GetTicks() / 1000.0;
  const auto frame_time = new_time - current_time_;

  current_time_ = new_time;
  accumulator_ += frame_time;
}

auto Timestep::integrate(const Integrator &integrator) noexcept -> void {
  while (accumulator_ >= delta_time_) {
    integrator(time_, delta_time_);

    accumulator_ -= delta_time_;
    time_ += delta_time_;
  }
}

class Sound::Impl final {
  Mix_Chunk *chunk_{nullptr};

public:
  Impl() noexcept = delete;
  Impl(const Impl &) noexcept = delete;
  Impl(Impl &&) noexcept = default;
  auto operator=(const Impl &) noexcept -> Impl & = delete;
  auto operator=(Impl &&) noexcept -> Impl & = default;
  ~Impl() noexcept;

  explicit Impl(const std::string &file);

  auto chunk() const noexcept -> const Mix_Chunk *;
  auto chunk() noexcept -> Mix_Chunk *;
};

Sound::Impl::~Impl() noexcept { Mix_FreeChunk(chunk_); }

Sound::Impl::Impl(const std::string &file) {
  chunk_ = Mix_LoadWAV(file.c_str());

  if (chunk_ == nullptr) {
    throw Error(Mix_GetError());
  }
}

auto Sound::Impl::chunk() const noexcept -> const Mix_Chunk * { return chunk_; }

auto Sound::Impl::chunk() noexcept -> Mix_Chunk * { return chunk_; }

Sound::Sound(const std::string &file) : impl_(new Impl(file)) {}

auto Sound::impl() const noexcept -> const Impl & { return *(impl_.get()); }

auto Sound::impl() noexcept -> Impl & { return *(impl_.get()); }

auto volume(Sound &sound, const std::size_t volume) noexcept -> void {
  Mix_VolumeChunk(sound.impl().chunk(), static_cast<int>(volume));
}

auto play(Sound &sound, const std::int32_t loops) -> Channel {
  const auto channel = Mix_PlayChannel(-1, sound.impl().chunk(), loops);

  if (channel == -1) {
    throw Error(Mix_GetError());
  }

  return static_cast<Channel>(channel);
}

auto play_timed(Sound &sound, const std::size_t milliseconds) -> Channel {
  const auto channel = Mix_PlayChannelTimed(-1, sound.impl().chunk(), -1, static_cast<int>(milliseconds));

  if (channel == -1) {
    throw Error(Mix_GetError());
  }

  return static_cast<Channel>(channel);
}

auto pause(const Channel channel) noexcept -> void { Mix_Pause(static_cast<int>(channel)); }

auto resume(const Channel channel) noexcept -> void { Mix_Resume(static_cast<int>(channel)); }

auto stop(Channel channel) noexcept -> void { Mix_HaltChannel(static_cast<int>(channel)); }

auto pause_all_channels() noexcept -> void { Mix_Pause(-1); }

auto resume_all_channels() noexcept -> void { Mix_Resume(-1); }

auto stop_all_channels() noexcept -> void { Mix_HaltChannel(-1); }

auto playing(Channel channel) noexcept -> bool { return Mix_Playing(static_cast<int>(channel)) != 0; }

class Font::Impl final {
  TTF_Font *font_{nullptr};

public:
  Impl() noexcept = delete;
  Impl(const Impl &) noexcept = delete;
  Impl(Impl &&) noexcept = default;
  auto operator=(const Impl &) noexcept -> Impl & = delete;
  auto operator=(Impl &&) noexcept -> Impl & = default;
  ~Impl() noexcept;

  Impl(const std::string &file, std::size_t size);

  auto font() const noexcept -> const TTF_Font *;
  auto font() noexcept -> TTF_Font *;
};

Font::Impl::~Impl() noexcept { TTF_CloseFont(font_); }

Font::Impl::Impl(const std::string &file, const std::size_t size) {
  font_ = TTF_OpenFont(file.c_str(), static_cast<int>(size));

  if (font_ == nullptr) {
    throw Error(TTF_GetError());
  }
}

auto Font::Impl::font() const noexcept -> const TTF_Font * { return font_; }

auto Font::Impl::font() noexcept -> TTF_Font * { return font_; }

Font::Font(const std::string &file, const std::size_t size) : impl_(new Impl(file, size)) {}

auto Font::impl() const noexcept -> const Impl & { return *(impl_.get()); }

auto Font::impl() noexcept -> Impl & { return *(impl_.get()); }

auto draw(Display &display, Font &font, const std::string &text, const Color &color, const Rectangle &source,
          const Rectangle &destination, const double angle, const Point &center, const bool blended) -> void {
  const auto sdl_source =
      SDL_Rect{static_cast<int>(source.point().x()), static_cast<int>(source.point().y()),
               static_cast<int>(source.dimension().width()), static_cast<int>(source.dimension().height())};

  const auto sdl_destination =
      SDL_Rect{static_cast<int>(destination.point().x()), static_cast<int>(destination.point().y()),
               static_cast<int>(destination.dimension().width()), static_cast<int>(destination.dimension().height())};

  const auto sdl_center = SDL_Point{static_cast<int>(center.x()), static_cast<int>(center.y())};

  const auto sdl_color = SDL_Color{color.red(), color.green(), color.blue(), color.alpha()};

  auto surface = static_cast<SDL_Surface *>(nullptr);

  if (blended) {
    surface = TTF_RenderText_Solid(font.impl().font(), text.c_str(), sdl_color);
  } else {
    surface = TTF_RenderText_Blended(font.impl().font(), text.c_str(), sdl_color);
  }

  if (surface == nullptr) {
    throw Error(TTF_GetError());
  }

  auto texture = SDL_CreateTextureFromSurface(display.impl().renderer(), surface);

  if (texture == nullptr) {
    throw Error(SDL_GetError());
  }

  SDL_FreeSurface(surface);

  if (blended) {
    if (SDL_SetRenderDrawBlendMode(display.impl().renderer(), SDL_BLENDMODE_BLEND) < 0) {
      throw Error(SDL_GetError());
    }
  } else {
    if (SDL_SetRenderDrawBlendMode(display.impl().renderer(), SDL_BLENDMODE_NONE) < 0) {
      throw Error(SDL_GetError());
    }
  }

  if (SDL_RenderCopyEx(display.impl().renderer(), texture, &sdl_source, &sdl_destination, degrees(angle), &sdl_center,
                       SDL_FLIP_NONE) < 0) {
    throw Error(SDL_GetError());
  }
}

auto draw(Display &display, Font &font, const std::string &text, const Color &color, const Point &point,
          const double angle, const double scale, const bool blended) -> void {
  const auto sdl_color = SDL_Color{color.red(), color.green(), color.blue(), color.alpha()};

  auto surface = static_cast<SDL_Surface *>(nullptr);

  if (blended) {
    surface = TTF_RenderText_Solid(font.impl().font(), text.c_str(), sdl_color);
  } else {
    surface = TTF_RenderText_Blended(font.impl().font(), text.c_str(), sdl_color);
  }

  if (surface == nullptr) {
    throw Error(TTF_GetError());
  }

  auto texture = SDL_CreateTextureFromSurface(display.impl().renderer(), surface);

  if (texture == nullptr) {
    throw Error(SDL_GetError());
  }

  if (blended) {
    if (SDL_SetRenderDrawBlendMode(display.impl().renderer(), SDL_BLENDMODE_BLEND) < 0) {
      throw Error(SDL_GetError());
    }
  } else {
    if (SDL_SetRenderDrawBlendMode(display.impl().renderer(), SDL_BLENDMODE_NONE) < 0) {
      throw Error(SDL_GetError());
    }
  }

  const auto scaled =
      Dimension({static_cast<std::size_t>(surface->w * scale), static_cast<std::size_t>(surface->h * scale)});

  const auto center = Point(scaled.width() / 2.0, scaled.height() / 2.0);

  const auto destination = Rectangle(point - Point(scaled.width() / 2.0, scaled.height() / 2.0), scaled);

  const auto sdl_source = SDL_Rect{0, 0, surface->w, surface->h};

  const auto sdl_destination =
      SDL_Rect{static_cast<int>(destination.point().x()), static_cast<int>(destination.point().y()),
               static_cast<int>(destination.dimension().width()), static_cast<int>(destination.dimension().height())};

  const auto sdl_center = SDL_Point{static_cast<int>(center.x()), static_cast<int>(center.y())};

  SDL_FreeSurface(surface);

  if (SDL_RenderCopyEx(display.impl().renderer(), texture, &sdl_source, &sdl_destination, degrees(angle), &sdl_center,
                       SDL_FLIP_NONE) < 0) {
    throw Error(SDL_GetError());
  }
}
} // namespace sdlx
